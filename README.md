# running iq as a docker container

https://github.com/microsoft/iqsharp/#using-iq-as-a-container



Having a look at Microsoft.Quantum.Sdk and to Quantum Development Kit samples

https://docs.microsoft.com/en-us/samples/browse/?products=qdk

https://marketplace.visualstudio.com/items?itemName=quantum.quantum-devkit-vscode

using iq as a container ( jupyter python3 )

https://github.com/microsoft/iqsharp/#using-iq-as-a-containerq

# Running some quantum simulations

```qs
namespace Qrng {
    open Microsoft.Quantum.Convert;
    open Microsoft.Quantum.Math;
    open Microsoft.Quantum.Measurement;
    open Microsoft.Quantum.Canon;
    open Microsoft.Quantum.Intrinsic;
    
    operation SampleQuantumRandomNumberGenerator() : Result {
        using (q = Qubit())  {  // Allocate a qubit.
            H(q);               // Put the qubit to superposition. It now has a 50% chance of being 0 or 1.
            return MResetZ(q);  // Measure the qubit value.
        }
    }

    operation SampleRandomNumberInRange(max : Int) : Int {
        mutable bits = new Result[0];
        for (idxBit in 1..BitSizeI(max)) {
            set bits += [SampleQuantumRandomNumberGenerator()];
        }
        let sample = ResultArrayAsInt(bits);
        return sample > max
               ? SampleRandomNumberInRange(max)
               | sample;
    }
    
    @EntryPoint()
    operation SampleRandomNumber() : Int {
        let max = 50;
        Message($"Sampling a random number between 0 and {max}: ");


        // jovyan@f89059706781:~/agarcia$ dotnet run
        // it outputs a random number
        return SampleRandomNumberInRange(max);
    }
}
```

## What does Grover's search algorithm do?

Grover's algorithm asks whether an item in a list is the one we are searching for. It does this by constructing a quantum superposition of the indexes of the list with each coefficient, or probability amplitude, representing the probability of that specific index being the one you are looking for.

At the heart of the algorithm are two steps that incrementally boost the coefficient of the index that we are looking for, until the probability amplitude of that coefficient approaches one.

The number of incremental boosts is fewer than the number of items in the list. This is why Grover's search algorithm performs the search in fewer steps than any classical algorithm.

## Running on Azure

https://info.microsoft.com/LearnMoreAboutMicrosoftQuantumNetwork.html